import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

export const LoginButton = () => {
  const { loginWithRedirect } = useAuth0();

  return (
    <div className="Login">
      <h2>Demo React Auth0</h2>
      <div className="logos">
        <img
          src="https://miro.medium.com/max/3840/0*oZLL-N4dGNlBe4Oh.png"
          alt="React"
        />
        <img
          src="https://bcrnews.com.ar/wp-content/uploads/2021/03/auth0-logo-whitebg.png"
          alt="Auth0"
        />
      </div>
      <br />
      <button onClick={() => loginWithRedirect()}>Login</button>
    </div>
  );
};
