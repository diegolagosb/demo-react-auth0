import logo from "./logo.svg";
import "./App.css";
import "./Login.css";
import { LoginButton } from "./Login";
import { LogoutButton } from "./Logout";
import { Profile } from "./Profile";
import { useAuth0 } from "@auth0/auth0-react";

function App() {
  const { isAuthenticated, isLoading } = useAuth0();
  return (
    <div className="App">
      <header className="App-header">
        {isAuthenticated ? (
          <>
            <Profile />
            <LogoutButton />
          </>
        ) : isLoading ? (
          <>
          <img src={logo} className="App-logo" alt="logo" />
          <div>Loading...</div>
          </>
        ) : (
          <LoginButton />
        )}
      </header>
    </div>
  );
}

export default App;
